<?php
// $Id$

/**
 * @file
 * Robax module for Drupal
 */
class Robax
{
  /**
   * Singleton pattern
   */
  protected static $instance = NULL;

  /**
   * Module directory
   */
  protected $module_dir = NULL;

  /**
   * Singleton pattern
   */
  public static function get_instance()
  {
    if (is_null(self::$instance))
    {
      self::$instance = new Robax();
    }

    return self::$instance;
  }

  /**
   * Constructor
   */
  protected function __construct()
  {
    $this->module_dir = drupal_get_path('module', 'robax');
  }

  /**
   * Resets module settings
   */
  public function reset_settings()
  {
    variable_del('robax_license');
  }

  /**
   * License number validation
   */
  public function validate_license($license)
  {
	if ($license === '') return false;

    return true;
  }
  
  /**
   * Checks if Robax settings are properly set up
   */
  public function is_installed()
  {
    $robax_license = variable_get('robax_license');

	if (is_null($robax_license)) return FALSE;

    if ($this->validate_license($robax_license) == FALSE) return FALSE;


    return TRUE;
  }

  /**
   * Checks if Robax tracking code is installed properly
   */
  public function tracking_code_installed()
  {
    if ($this->is_installed() == FALSE) return FALSE;

    return TRUE;
  }

  public function install_codes()
  {

      $license = variable_get('robax_license');

      drupal_add_js('https://callback.oblax.ru/widget-get?key='.$license, array(
          'type' => 'external',
          'scope' => 'footer',
      ));
  }


  /**
   * Includes admin CSS file
   */
  public function include_admin_css()
  {
    drupal_add_css($this->module_dir . '/css/robax.css');
  }


}