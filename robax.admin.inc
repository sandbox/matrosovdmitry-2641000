<?php
// $Id$

/**
 * @file
 * Robax module admin panel
 */

function robax_admin_settings_form($form_state) {
  global $base_url;

    $robax = Robax::get_instance();
    $robax->include_admin_css();


  /**
   * Tracking code notification
   */
  if ($robax->tracking_code_installed()) {
    $form['tracking_code'] = array
    (
      '#type' => 'item',
      '#markup' => t('<div class="messages installed_ok">Robax успешно установлен к вам на сайт.</div>')
    );
  }


	  $form['choose_form'] = array
	  (
		'#type' => 'item',
		'#markup' =>

        '<div>'.
					'Для того, что бы <a target="_blank" href="http://robax.oblax.ru/">Robax</a> работал на вашем сайте, вам надо:<br>'.
					'<ul style="list-style-type: decimal;">'.
						'<li><a target="_blank" href="http://lk.oblax.ru">Зарегистрироваться в сервисе</a></li>'.
                        '<li>Создать виджет</li>'.
                        '<li>Получить идентификатор виджета (вы сможете найти идентификатор в настройках виджета, или получить его при добавлении виджета в систему)</li>'.
						'<li>Ввести идентификатор вижета в поле на этой странице</li>'.
						'<li>Нажать кнопку "Сохранить"</li>'.
					'</ul>'.
                    'Настроить работу сервиса вы можете в настройках сайта, в вашем <a target="_blank" href="http://lk.oblax.ru">личном кабинете</a>'.
				'</div>'
	  );

	  // General Settings
	  $form['general'] = array
	  (
		'#type' => 'fieldset',
		'#collapsible' => FALSE,
		'#prefix' => '<div id="robax_already_have"><h3>Детали аккаунта</h3>',
		'#suffix' => '</div>'
	  );

	  $form['general']['robax_login'] = array(
		'#type' => 'textfield',
		'#title' => t('Идентификатор виджета'),
		'#default_value' => variable_get('robax_license'),
		'#size' => 30,
		'#maxlength' => 100,
		'#required' => FALSE // handled by JavaScript validator
	  );

  return system_settings_form($form);
}

function robax_admin_settings_form_validate($form, &$data) {
    $robax = Robax::get_instance();

  // Check if "Reset" button has been clicked
  if (isset($data['input']) && isset($data['input']['op']) && $data['input']['op'] === 'Reset settings')
  {
      $robax->reset_settings();
  }
  else
  {
	  /**
	   * Form validation
	   */
	  if ($robax->validate_license($data['input']['robax_login']) == FALSE) {
		form_set_error('robax_license_number', t('Robax license number is incorrect.'));
	  }

	  variable_set('robax_license', $data['input']['robax_login']);
  }
}