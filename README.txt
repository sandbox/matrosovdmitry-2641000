// $Id$

Installing Robax in Drupal:

1. Upload `Robax` directory to `/sites/all/modules/`.
2. Open your Drupal website.
3. Go to "Administration > Modules" and activate `Robax` module (under `Other` category).
4. Go to "Administration > Robax" and follow the instructions.

---
For further information, please visit our website:
robax.oblax.ru
